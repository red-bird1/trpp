import sys
import datetime

arr = ['mm', 'sm', 'dm', 'm', None, None, 'km']


def convert(valuetmp, param1, param2):
    if None in (valuetmp, param1, param2):
        raise IOError("Invalid amount of arguments, it must be 3: value, from, to")
    if not valuetmp.isdigit():
        raise IOError("Invalid 1st arg")
    else:
        valuetmp = int(valuetmp)
    try:
        param1 = arr.index(param1)
    except:
        raise IOError("Invalid 2nd arg")
    try:
        param2 = arr.index(param2)
    except:
        raise IOError("Invalid 3rd arg")

    diff = int(param2) - int(param1)
    if int(diff) > 0:
        return valuetmp * 10 ** int(diff)
    elif int(diff) < 0:
        return valuetmp * 10 ** int(diff)
    else:
        return valuetmp


def save_convert(valuetmp, param1, param2):
    res = convert(valuetmp, param1, param2)
    ct = datetime.datetime.now()
    f = open("results.txt", "a")
    f.write(f'{ct} | Value {valuetmp} from {param1} to {param2} = {res}\n')
    f.close()
    return res


def main():
    if len(sys.argv) != 4:
        raise IOError("Invalid amount of arguments, it must be 3: value, from, to")
    value = sys.argv[1]
    measure_1 = sys.argv[2]
    measure_2 = sys.argv[3]
    print(f'Value {value} from {measure_1} to {measure_2} =', convert(value, measure_1, measure_2))


if __name__ == "__main__":
    main()
