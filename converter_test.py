import pytest
from converter import save_convert


def test_mm_to_sm():
    assert save_convert('1000', 'mm', 'sm') == 100


def test_sm_to_mm():
    assert save_convert('100', 'sm', 'mm') == 1000


def test_dm_to_km():
    assert save_convert('10000', 'dm', 'km') == 1


def test_m_to_mm():
    assert save_convert('10', 'm', 'mm') == 10000


def test_invalid_from():
    with pytest.raises(Exception):
        convert('10', 'asd', 'fas')
